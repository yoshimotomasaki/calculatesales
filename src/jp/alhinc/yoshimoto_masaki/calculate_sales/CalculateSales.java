package jp.alhinc.yoshimoto_masaki.calculate_sales;
package jp.alhinc.calculate_sales.yoshimoto_masaki;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;


public class CalculateSales {

    public static void main(String[] args){
        try {
            Map<String, String> branchInfo = new HashMap<>();//店舗情報
            Map<String, Long> salesInfo = new HashMap<>();//売上情報

            BufferedReader br = null;
            File branchLst = new File(args[0], "branch.lst");

            if(!branchLst.exists()) {
                System.out.println("支店定義ファイルが存在しません");
                return;
            }

            try{
                br = new BufferedReader(new FileReader(branchLst));
                String line;
                
                while((line = br.readLine()) != null){
                    String[] branch = line.split(",", -1);
                    if(branch.length != 2 || !branch[0].matches("^[0-9]{3}$")
                            || !branch[1].replaceAll("　", " ").trim().isEmpty()){
                        System.out.println("支店定義ファイルのフォーマットが不正です");
                        return;
                    }

                    branchInfo.put(branch[0], branch[1]);//支店マップに書き込み
                    salesInfo.put(branch[0], 0L);//売上マップに書き込み
                }
            }finally{
                br.close();
            }

            File directory = new File(args[0]);
            File[] rcdFiles = directory.listFiles((dir, name) -> {
                return new File(dir, name).isFile() && name.matches("^\\d{8}.rcd$");
            });

            if (rcdFiles.length == 0) {
                System.out.println("予期せぬエラーが発生しました");
                return;
            }

            Arrays.sort(rcdFiles);

            int min = Integer.parseInt(rcdFiles[0].getName().substring(0, 8));
            int max = Integer.parseInt(rcdFiles[rcdFiles.length - 1].getName().substring(0, 8));

            if (min + rcdFiles.length - 1 != max) {
                System.out.println("売上ファイル名が連番になっていません");
                return;
            }

            BufferedReader br2 = null;
            for(File rcdFile : rcdFiles){
                try {
                    br2 = new BufferedReader(new FileReader(rcdFile));
                    String code = br2.readLine();
                    if(!branchInfo.containsKey(code)){
                        System.out.println(rcdFile.getName() + "の支店コードが不正です");
                        return;
                    }
                    Long sales = Long.parseLong(br2.readLine());
                    if(br2.readLine() != null){//売上ファイル3行目チェック
                        System.out.println(rcdFile.getName() + "のフォーマットが不正です");
                        return;
                    }
                    Long total = sales + salesInfo.get(code);//コードごとに合計
                    if(sales.toString().length() > 10){//合計金額10桁か
                        System.out.println("合計金額が10桁を超えました");
                        return;
                    }
                    salesInfo.put(code, total);//売上マップに書き込み
                }finally{
                    br2.close();
                }
            }

            FileWriter fw = null;
            try{
                fw = new FileWriter(new File(args[0], "branch.out"));
                for(Entry<String, String> entry : branchInfo.entrySet()){
                    String code = entry.getKey();
                    String name = entry.getValue();
                    Long total = salesInfo.get(code);
                    fw.write(code + "," + name + "," + total + "\r\n");
                }
            }finally{
                fw.close();
            }
        } catch (Exception e) {
            System.out.println("予期せぬエラーが発生しました");
        }
    }
}
